package pe.edu.uni.fiis.venta.service;

import pe.edu.uni.fiis.venta.model.Usuario;

public interface UsuarioService {
    public Usuario agregarUsuario(Usuario usuario);

}
