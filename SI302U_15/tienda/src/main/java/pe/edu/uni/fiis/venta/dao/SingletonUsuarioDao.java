package pe.edu.uni.fiis.venta.dao;

import pe.edu.uni.fiis.venta.dao.usuario.UsuarioDao;
import pe.edu.uni.fiis.venta.dao.usuario.UsuarioDaoImpl;

public abstract class SingletonUsuarioDao {
    private static UsuarioDao usuarioDao = null;
    public static UsuarioDao getUsuarioDao(){
        if(usuarioDao == null){
            usuarioDao = new UsuarioDaoImpl();
        }
        return usuarioDao;
    }
}

