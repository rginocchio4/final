package pe.edu.uni.fiis.venta.controller;

import pe.edu.uni.fiis.venta.model.Usuario;
import pe.edu.uni.fiis.venta.service.SingletonService;
import pe.edu.uni.fiis.venta.util.Json;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "UsuarioController",urlPatterns = {"/registro-usuario"})
public class UsuarioController extends HttpServlet {

    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String nombre=req.getParameter("nom");
        String dni=req.getParameter("dni");
        String celular=req.getParameter("cel");
        String rol=req.getParameter("rol");
        Usuario usur1= new Usuario(nombre,dni,celular,rol);

        SingletonService.getUsuarioService().agregarUsuario(usur1);
        resp.getWriter().write("Se guardo el usuario correctamente");
    }

    public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String data = Json.getJson(req);

        Usuario usuario = Json.getInstance().readValue(data, Usuario.class);

        usuario = SingletonService.getUsuarioService().agregarUsuario(usuario);
        Json.envioJson(usuario,resp);
    }
    public static void main(String[] args) {
    }
}
