package pe.edu.uni.fiis.venta.service;

public class SingletonService {
    private static UsuarioService usuarioService = null;

    public static UsuarioService getUsuarioService() {
        if (usuarioService == null) {
            usuarioService = new UsuarioServiceImpl();
        }
        return usuarioService;
    }
}
