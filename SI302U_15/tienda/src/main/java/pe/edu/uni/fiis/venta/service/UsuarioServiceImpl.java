package pe.edu.uni.fiis.venta.service;

import pe.edu.uni.fiis.venta.dao.SingletonUsuarioDao;
import pe.edu.uni.fiis.venta.model.Usuario;

import java.sql.Connection;
import java.sql.SQLException;

public class UsuarioServiceImpl implements UsuarioService {
    public Usuario agregarUsuario(Usuario usuario) {
        Connection connection = Conexion.getConnection();
        Usuario usur = SingletonUsuarioDao.getUsuarioDao().agregarUsuario(usuario, connection);
        try {
            connection.commit();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usur;
    }
}

