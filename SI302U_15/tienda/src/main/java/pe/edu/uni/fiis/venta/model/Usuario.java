package pe.edu.uni.fiis.venta.model;



public class Usuario {
    private String nombre;
    private String dni;
    private String n_cel;
    private String rol;

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getN_cel() {
        return n_cel;
    }

    public void setN_cel(String n_cel) {
        this.n_cel = n_cel;
    }

    public Usuario(String nombre, String dni, String n_cel, String rol) {
        this.nombre = nombre;
        this.dni = dni;
        this.n_cel = n_cel;
        this.rol = rol;
    }
}