package pe.edu.uni.fiis.venta.dao.usuario;

import pe.edu.uni.fiis.venta.model.Usuario;
import pe.edu.uni.fiis.venta.service.Conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class UsuarioDaoImpl implements UsuarioDao {
    public Usuario agregarUsuario(Usuario a, Connection b) {

        try {
            StringBuffer sql = new StringBuffer();
            sql.append("insert into usuario (nombre,dni,ncel,rol) values (")
                    .append("?,?,?,?)");

            PreparedStatement sentencia = b.prepareStatement(sql.toString());
            sentencia.setString(1, a.getNombre());
            sentencia.setString(2, a.getDni());
            sentencia.setString(3, a.getN_cel());
            sentencia.setString(4, a.getRol());


            sentencia.execute();
        } catch (SQLException e) {
            e.printStackTrace();

        }
        return a;

    }


    /*public static void main(String[] args) {
        //Conexion con = new Conexion();
        Connection c = Conexion.getConnection();
        try {
            System.out.println(c.isValid(5000));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        UsuarioDaoImpl usuariodao = new UsuarioDaoImpl();
        Usuario usuario = new Usuario("Rosa", "72429954", "973819459", "Comprador");
        usuariodao.agregarUsuario(usuario, c);
        try {
            //c.commit();
            c.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }*/
}