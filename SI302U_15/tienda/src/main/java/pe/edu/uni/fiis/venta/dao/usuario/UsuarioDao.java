package pe.edu.uni.fiis.venta.dao.usuario;

import pe.edu.uni.fiis.venta.model.Usuario;

import java.sql.Connection;

public interface UsuarioDao {
    public Usuario agregarUsuario(Usuario a, Connection b);
}
